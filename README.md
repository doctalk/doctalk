<img src="documentation/DocTalk-Logo.png" height="20" align="right" /> 

# DocTalk: Conversational Agent for Reflective Learning

This GitLab repository contains the code and of documentation of the conversational agent for reflective learning as part of the research project DocTalk. In the repository, the term DocTalk usually refers to the conversational agent itself. 

_(The more detailed former project wiki including sensitive content is available to project members in a [new repository](https://git.imp.fu-berlin.de/doctalk/doctalk_wiki). The original non-open-source repository including the project history is available to project members [here](https://git.imp.fu-berlin.de/doctalk/doctalk-original/).)_

# Table of Contents

[[_TOC_]]

# Research Project DocTalk

DocTalk is a BMBF-funded collaborative research project by the Charité - Universitätsmedizin Berlin, the FernUniversität in Hagen and the Freie Universität Berlin. The overall objective of the project "DocTalk - Dialog trifft Chatbot" is to test digital communication and learning paths in order to meet the increased requirements of interdisciplinary cooperation and at the same time specialised process flows both technically and didactically. You can find the extended project description in the internet appearance of the research group Human-Centered Computing (Freie Universität Berlin) [here](https://www.mi.fu-berlin.de/en/inf/groups/hcc/research/projects/doctalk/index.html).

In order to achieve the set goals, a pro-active communication platform is implemented at the Charité, including a conversational agent which is supposed to support reflective learning processes of assistant physicians in the clinical setting. The research group Human-Centered Computing designs and implements the conversational agent in collaboration with the project partners and the assistant physicians as end users of the pro-active communication platform. In this research project, we make use of a participatory design approach which understands the users of software products as experts in their respective domain. Therefore, in participatory design, the user is given the opportunity to contribute to and influence the software outcome in an iterative, collaborative and democratic design process. \[[1](https://git.imp.fu-berlin.de/doctalk/doctalk/-/blob/master/README.md#references)\]

# Partners in the Research Project DocTalk

The research project DocTalk is pursued by the following three project partners:

**DocTalk Project Coordinator / Context**\
Charité – Universitätsmedizin Berlin\
Medizinische Klinik mit Schwerpunkt Psychosomatik\
Prof. Dr. med. M. Rose\
https://psychosomatik.charite.de/metas/person/person/address_detail/rose-1/

**Subproject Human-Centered Computing**\
Freie Universität Berlin\
Research Group HCC\
Prof. Dr. C. Müller-Birn\
http://www.mi.fu-berlin.de/en/inf/groups/hcc/members/professor/mueller-birn.html

**Subproject Media Didactics / Media Education**\
FernUniversität in Hagen\
Mediendidaktik\
Prof. Dr. S. Hofhues\
https://www.fernuni-hagen.de/bildungswissenschaft/mediendidaktik/team/sandra.hofhues.shtml


# How to use the DocTalk Chatbot Based on Rasa Open Source

Rasa provides developers with a default chatbot project as well as guidelines for setting up one's own chatbot, training it, and including specific features.
The DocTalk chatbot is based on the initial English default chatbot and iteratively adjusted for the purposes of DocTalk and german language.

This repository is compatible with Rasa Open Source version 2.x. The exact version used in the project is Rasa OS 2.0.2.

For a brief introduction to building chatbots with Rasa 2.x, please check out the Rasa series on Conversational AI with Rasa Open Source 2.x:

[![](http://img.youtube.com/vi/Ap62n_YAVZ8/0.jpg)](https://www.youtube.com/watch?v=Ap62n_YAVZ8&list=PL75e0qA87dlErcimrWHKEUq4b-x9PrpLe&index=1)


## Rasa Channel Connectors

Rasa Open Source provides many built-in connectors to connect to common messaging and voice channels, like Slack, Telegram, Facebook Messenger etc.
These channels allow the Rasa chatbot to receive messages from users and respond to them through the corresponding platform or channel. To connect Rasa to a specific channel, you will need to configure the channel by providing the necessary credentials, such as API keys or tokens.
Rasa also allows you to create a custom channel by writing a connector that implements the necessary logic to receive and send messages through a specific channel (see [Rasa Docs](https://rasa.com/docs/rasa/messaging-and-voice-channels/)).

DocTalk is available on [Mattermost](https://docs.mattermost.com/guides/deployment.html). You can read more about setting Mattermost [here](https://rasa.com/docs/rasa/connectors/mattermost/).



# How to run the DocTalk Chatbot locally

This guideline briefly describes, how to run the chatbot locally.

**Prerequisites**

- new virtual environment (here: "venv")
- start: e.g. [anaconda3](https://www.anaconda.com/) (conda 4.9.2) environment with python 3.7.9, pip 20.2.4
- Rasa 2.1.0, Rasa SDK 2.2.0, Rasa X 0.34.0 (not necessary for local setup)
- [spaCy](https://spacy.io/) 2.3.5
- [Docker](https://www.docker.com/)


**Steps:**

1. In terminal, navigate to empty directory where the GitLab branch shall be cloned to.
2. Clone branch from doctalk repository
**`git clone -b develop --single-branch https://git.imp.fu-berlin.de/doctalk/doctalk.git`**  <br>
then navigate to the _doctalk_ directory:
**`cd doctalk`**
3. Activate virtual environment: **`conda activate venv`**
4. There is a docker container [rasa/duckling](https://hub.docker.com/r/rasa/duckling) available. Start duckling server in _a new terminal window_, simply running:
**`docker run -p 8000:8000 rasa/duckling`**
5. Train model: **`rasa train`**
6. Start action server in a new terminal window: 
    
    - Navigate to the chatbot directory where the folder _action_ is located: **`cd /<path>/doctalk`** <br>
    - Activate virtual environment: **`conda activate venv`**  <br>
    - Start action server: **`rasa run actions`** (if you experience problems with action display in _rasa shell_, use **`rasa run actions --actions actions`** instead  <br>

7. Run Rasa Shell: **`rasa shell`** and enjoy your conversation with the chatbot :wink:


# Chatbot Documentation

In DocTalk, the chatbot development process is built using Rasa, an open-source framework. Rasa offers developers a pre-configured chatbot project as a starting point and provides detailed instructions for creating and customizing a chatbot, training it, and incorporating desired features.
You can find further information about the Rasa framework in [Rasa Docs](https://rasa.com/docs/rasa/2.x)
We outlined the requirement analysis and choice of chatbot architecture for the chatbot in our technical report \[[7](https://git.imp.fu-berlin.de/doctalk/doctalk/-/blob/master/README.md#references)\]. You can find a work in progress version of the technical report [here](https://git.imp.fu-berlin.de/doctalk/doctalk/-/blob/master/documentation/Technical_Report_V_0.9.pdf).


## Overview of the Included Chatbot Files

<kbd>data/nlu.yml</kbd> - contains NLU training data for predicting the user's intention (_also:_ intent) based on the user's utterance

<kbd>data/rules.yml</kbd> - contains rules that the chatbot must obeye during its interaction with the user

<kbd>data/stories.yml</kbd> - contains stories training data for predicting the best chatbot reaction based on its previous interaction with the user

<kbd>actions/actions.py</kbd> - contains custom action code, i.e., python code for complex chatbot reactions that go beyond pure text responses, e.g., information retrieval through connectors to databases and API calls

<kbd>domain.yml</kbd> - represents the chatbot's central information storage on all included intents, entities, responses (including the response texts), actions, and forms

<kbd>config.yml</kbd> - contains the training configurations for the NLU pipeline, including the applied language model, interim intent and entity extractors, and applied Rasa policies


## Chatbot Architecture

Rasa Open Source has a scalable architecture. The main components are Natural Language Understanding (NLU), Dialogue Management(DM) and Natural Language Generation (NLG): 
- NLU component processes and interprets user inputs, extracting relevant information such as intent and entities.
- DM component uses the extracted information to determine the appropriate response to the user's input and maintain the context of the conversation.
- NLG component generates the text of the chatbot's response.


![Chatbot Architecture](documentation/chatbot_architecture.png)

Rasa Open Source provides many built-in connectors to connect to common messaging and voice channels, like Slack, Telegram, Facebook Messenger etc.
These channels allow the Rasa chatbot to receive messages from users and respond to them through the corresponding platform or channel. To connect Rasa to a specific channel, you will need to configure the channel by providing the necessary credentials, such as API keys or tokens.
Rasa also allows you to create a custom channel by writing a connector that implements the necessary logic to receive and send messages through a specific channel (see [Rasa Docs](https://rasa.com/docs/rasa/messaging-and-voice-channels/)).


In our case, we connected our chatbot to [Mattermost](https://mattermost.com/) (as shown in the above figure). You can read more about setting Mattermost [here](https://rasa.com/docs/rasa/connectors/mattermost/). 
 
Additionally, Rasa allows for easy integration with other AI services and external APIs. Since the software is intended for use by medical organizations (in our case Charité), it was necessary to integrate medical external sources like AMBOSS and Charité internal information sources. However, the open-source version of the chatbot is unable to connect to [AMBOSS](https://www.amboss.com/de/aerztinnen-aerzte?utm_source=google&utm_medium=cpc&utm_campaign=DE_DP_Brand_DP-PH_SEA_B_STD_0&utm_spc=brand&utm_2=DE_DP_Brand_DP-PH_SEA_B_STD_0_PUR_MIX_0_Brand-general&utm_3=amboss&utm_content=521993088602&utm_market=dp&utm_lng=de&gclid=CjwKCAjwquWVBhBrEiwAt1KmwsyTwwb4hNC9WSZ8RrZ1qJz8g3M62f0s4g6tMCKOX2X_05PGmL-MhRoCZngQAvD_BwE) and Charité sources due to the lack of a necessary token (AMBOSS membership is required) and GDPR compliance.


## The Scope of the DocTalk Chatbot

During the iterative chatbot design process, we found that the DocTalk chatbot as a pro-active chatbot for supporting sustainable, or else reflective, learning in residents, must cover three main interaction components:

1. Basic communication skills, e.g., greeting and farewell-wishing, replying to thank you, introducing oneself, explaining the scope of the chatbot, setting reminders, etc.
2. First-level support for residents, e.g., providing information on basic organizational and technical matters, including referring to more advanced internal and external data sources
3. Sustainable learning sessions, e.g., providing on-demand reflection sessions as well as providing materials and aggregated visualizations of stored spread information for review

This DocTalk chatbot repository serves as a conceptual approach to how a chatbot that supports sustainable learning in residents can be initialized. Thus, it demonstrates exemplary implementations for covering central scenarios and use cases that were initially set (use scenarios in DocTalk project application) or identified in the course of the DocTalk research project.

The DocTalk chatbot is designed for a German, clinical context with the following main scenarios:
- medical education and training: DocTalk aims to provide a platform for medical education and training, with the chatbot assisting in the delivery of educational content to medical professionals. This will include collecting feedback from users and using it to improve the content and delivery of the educational materials.

- internal communication and coordination: DocTalk will also serve as a communication and coordination tool for medical professionals within the clinic. The chatbot will facilitate the sharing of information and updates between medical staff, improving workflow and reducing the risk of errors or misunderstandings.


### Covered Scenarios and Functionalities

The final implementation of the DocTalk chatbot covers the following scenarios and Functionalities:

| Topic | User Need | Scenario | Source | Initiating Intent(s) |
| ------ | ------ | ------ | ------ | ------ |
| Welcome Message on Start | Users are unsure what to do upon conversation entrance if there is no welcome message by the chatbot | User opens the chatbot channel. The chatbot shows a welcome message incl. a brief presentation of the chatbot and its scope. In later conversations, the chatbot shows a briefer welcome message to ease up the interaction. | Project Team Internal User Study | upon conversation entrance |
| Basic Communication | Basic Communication | Lisa starts DocTalk expecting to be greeted, for the chatbot to master the minimum etiquette of "please" and "thank you" responses, for the chatbot to introduce itself, and for her to say goodbye to the chatbot. | Project Vision | `goodbye`, `hello`, `request_introduction_of_chatbot`, `thank_you`, `ask_for_scope_of_application` |
| Reminder Functionality | The user wants to set a reminder | - | Project Vision | `request_setting_a_reminder` |
| Application Scenario 1: Search Learning Materials in Amboss | Users submit a search request for learning materials to DocTalk and expect graphically prepared content, e.g. from Amboss, to be returned. | Lisa would like to learn more about "cluster headaches" and asks the chatbot what is available about it in Amboss. The chatbot provides Lisa with information from Amboss about cluster headaches. | Project Vision | `amboss` |
| Sick Note Instructions | The user wants to know how to inform the personell department of the Charité regarding sickness | - | Explorative Interviews with Project Team | `ask_for_sick_note_instructions` |
| Various FAQs | There are questions that residents frequently pose to their superiors or colleagues that require a stable and simple answer. | Lisa asks her nearby colleague for the number of the ward physician on duty in the surgery department. | Project Vision / Explorative Interviews with Project Team / Analysis of Chatprotocolls | retrieval intent: `faq`, topics: `ask_overview_mandatory_dates_in_psychosomatic_clinic`, `ask_anorexia_patient`, `ask_night_shift`, ... |
| Request Phone Numbers | Residents ask each other and related staff for phone numbers of individuals on a regular basis | - | Explorative Interviews with Project Team / Analysis of Chatprotocolls | `ask_for_phone_number` |


### Identified Features

In addition, the following scenarios/functionalities were identified as potentially beneficial in this context:

| Topic | User Need | Scenario | Source | Comment |
| ------ | ------ | ------ | ------ | ------ |
| Interactive Machine Learning | potential users are interested in how they can support improving the chatbot quality | Lisa would like to learn from the chatbot how she can help with the further development of the chatbot. For this, the chatbot explains that a channel named < DocTalk requests > can be created where Lisa writes requests to the chatbot when they occur to her during the daily clinic routine. The chatbot explains how these requests are collected and used for new dialogs with the chatbot. | User Meetings | - |
| Integration of approved external information materials | In the medical domain, there are standardized information sources that residents rely on, e.g., structured operating procedures ('SOP') for the clinic, binding guidelines for medical workflows ('AWMF-Leitlinen') issued by the Arbeitsgemeinschaft der Wissenschaftlichen Medizinischen Fachgesellschaften e. V. \[[8](https://git.imp.fu-berlin.de/doctalk/doctalk/-/blob/master/README.md#references)\], recommendations for decision making in medical decisions ('DGIM-Empfehlungen) 'issued by the Deutsche Gesellschaft für Innere Medizin \[[9](https://git.imp.fu-berlin.de/doctalk/doctalk/-/blob/master/README.md#references)\]. They are updated frequently, published in separate information systems, and made available in separate interactive applications. | - | Explorative Interviews with Project Team | Project focus on exemplary integration of Amboss as an external information source |
| Medication Dosage | Residents regularly look up information regarding the dosage of specific active ingredients ('LINA' and 'DINA'). | Lisa asks for the dosage of an active ingredient according to Amboss (DINA/LINA). The chatbot responds the DINA and LINA values according to Amboss. | Explorative Interviews with Project Team | Inspiration for what an advanced Amboss API could handle in the future. |
| Patient Handover | Get an overview of patient data using documented information in different information systems | Lisa starts her night shift. Her colleague hand over informations regarding the patients from the late shift and gives her information about the last decisions regarding their treatment plans and pending tests. Later on during the night shift, Lisa notices that she is missing information about the patient in room 2.06. She requests a patient handover for them in the chatbot. The chatbot gives her an overview of the latest information known about the patient via various channels. Lisa still has a few questions about this, but she can research these independently or document them again as an open issue for the early shift. | Explorative Interviews with Project Team / Analysis of Chatprotocolls | 1. A first design draft was developed for the patient handover dashboard. 2. A workaround solution using '#<patient_surname>' in Mattermost was established in the Charité project team, and supported in the chatbot by adding a documentation guide on request. |
| Inquire about symptoms of a disease | Users want to request symptoms that are associated with a disease as documented in Amboss | The user asks for symptoms of a disease. The chabot hands back the associated symptoms as documented in the disease's main article in Amboss | Project Vision | Inspiration for what an advanced Amboss API could handle in the future. |
| Transparency with LIME | The user requests how the chatbot understood the last request. | N asks how the chatbot understands content. The chatbot shows its processing based on an example user input and the LIME marker, which words contributed to the categorization of the current intent. | Project Vision | LIME can be implemented for the Rasa chatbot, however, the processing of the last user input and intent classification is too slow to enrich an ongoing conversation |
| Sustainable Learning, Reflection Dashboard | Gain an overview of one's own communication and requests in DocTalk to identify learning gaps. | Lena is a resident at Charité and uses the DocTalk system. During her onboarding / installation of the system, she decided to receive the user dashboard once a week on Wednesday at 7 p.m., because every Wednesday she tries to explicitly train to become a specialist in psychosomatic medicine and psychotherapy, i.e. she then completes her diary for the previous week and looks up content that she was not able to discuss with her supervisor last week. Today is Wednesday and Lena receives her personal user dashboard on her way home from the day shift. The report shows her that she most frequently requested factual knowledge about Amboss, asked for phone numbers three times, and did not use an on-demand reflection session. The report also shows her the topics she talked about most often, requests to look up records on diagnostic forms. Lena wants to know more about this and clicks on this information. She now gets a more detailed view with some examples of her requests. After a brief interaction with her user dashboard, Lena completes her diary for the past week. | Explorative Interviews with Project Team / User Meetings | A concept for a reflection dashboard was developed |
| Sustainable Learning, Reflection Sessions Using Scripts | Reflect on one's own actions with the help of a thematic reflection script in order to arrive at new insights in a sustainable way. | Lisa has seen in her weekly dashboard provided by DocTalk that she can participate in a reflection module on the topic of "Data protection in everyday clinical practice". She asks DocTalk about the module and activates it. Over the next three weeks, Lisa receives six scenario descriptions on the topic of "Data protection in everyday hospital life" and is then asked about her own experiences with this and her thoughts on it during a reflection session. After the last session, DocTalk indicates that the reflection module has been completed and encourages Lisa to continue thinking about this topic. | Project Vision | A concept for reflection sessions was developed |
| Application Scenario 2: Request Patient Data | Asking for patient information using contextual information. | User requests information regarding a patient. The chatbot interrogates, and makes use of other patient data (stored in clinical database), to come up with a more suitable response. | Project Vision | - |
| Application Scenario 3: Personalization | Personalization: The chatbot reacts in a different manner depending on the user group that a user belongs to | - | Project Vision | - |


The following features were also loosely discussed/mentioned in user studies: diagnosis or treatment planning based on medical history, findings, symptoms, and previous treatment history, integration of excel lists for outpatient and admission planning, support in swapping rosters, support in requesting free beds in other clinics, support in setting/planning/cancelling dates with colleagues, support in treatment planning (provide contextual information from predefined sources), request documents for specific patients (step-wise secure authorization, and multiple database accesses required), handle unknown user requests with a handover to a tool with a broad knowledge base, and connection to the external sources 'UpToDate', medication dosage applications, and locally stored excel sheets for organizational matters.

They were not further elaborated due to lack of feasibility (limited capacities), lack of expertise (advanced patient care relevant decision support), lack of project focus (simplification of multiple organizational tasks of residents, requests for useful minor simplications in everyday clinic life), lack of sustainability (generation of redundant information systems), lack of access (linked databases harbor data security risks, access rights must therefore be requested and organized at an early stage), lack of sustainability (during the project, a follow-up technology TI-Messenger was identified that will enable some of the organizational tasks), or lack of prioritization.

We hope that presenting the diversity of potential requests and sources that are relevant to answer them, informs future teams when desigining new tools and software for (and with!) residents.

## DocTalk Demo

The detailed interaction concept of the DocTalk chatbot can be taken from the illustrations in Appendix A. The following demo shows an exemplary interaction with the DocTalk chatbot:

![DocTalk Demo](documentation/doctalk_demo.gif)

# License

See [LICENSE](https://git.imp.fu-berlin.de/doctalk/doctalk/-/blob/develop/LICENSE) and [NOTICE](https://git.imp.fu-berlin.de/doctalk/doctalk/-/blob/develop/NOTICE) files.


# Acknowledgements 
<img src="documentation/BMBF_Logo_1000px.png" height="100" align="right" /> 
This work is supported by the German Federal Ministry of Education and Research, grant 01PG20002C ("DocTalk - Dialogue meets chatbot: Collaborative learning and teaching for doctors in the process of work").

# References

\[1\] Spinuzzi, Clay. 2005. ‘The Methodology of Participatory Design’. Technical Communication 52 (2): 163–74.

\[2\] Aschmann, Paul. _Rasa UI_. 2019 https://github.com/paschmann/rasa-ui

\[3\] Slack Technologies, Inc., _Slack_. 2022. https://slack.com/

\[4\] Telegram FZ-LLC, _Telegram_. 2022. https://telegram.org/

\[5\] Meta Platforms, Inc., _Facebook Messenger_. 2022. https://www.messenger.com/

\[6\] Mattermost, Inc. _Mattermost_. 2021. https://mattermost.com/

\[7\] Strama, Johann, et al. _DocTalk: Implementing an Open Source Conversational Agent Architecture in the Medical Domain - Unpublished Work in Progress_. Freie Universität Berlin, Fachbereich Mathematik und Informatik, Serie B, Informatik, 25 Sept. 2023.

\[8\] Arbeitsgemeinschaft der Wissenschaftlichen Medizinischen Fachgesellschaften (AWMF) - Ständige Kommission Leitlinien, editor. _AWMF-Regelwerk “Leitlinien.”_ 2nd ed., 2020.

\[9\] Deutsche Gesellschaft für innere Medizin e.V. “Empfehlungen.” _Klug Entscheiden_, 2022, https://www.klug-entscheiden.com/empfehlungen/uebersicht.


***

# Appendix A

## Interaction Concept Based on the Chatbot Rules

**Legend**

```mermaid
graph LR;
    subgraph Legend
        int(Intent):::intent
        act{{Action}}:::action
        resp(Response):::response
        dec{Decision<br>point}:::decision
    end

   %% diagram styling
    classDef intent fill:#A0DAA9, stroke:#000000;
    classDef action fill:#FDAC53, stroke:#000000;
    classDef decision fill:#98B4D4, stroke:#000000;
    classDef response fill:#FFFFDB, stroke:#FE840E;
    classDef f fill:#ecffb3, stroke:#000000;
    class Legend f

```

**Interaction Concept**

```mermaid
flowchart TB;
    S[START] --> out_of_scope_eingabe(Out-of-scope<br>Input):::intent
    S --> nlu_fallback(Fallback):::intent
    style S fill:#939597

    %% out_of_scope_eingabe and nlu_fallback 
    out_of_scope_eingabe(Out-of-scope<br>Input) --> action_reaction_to_setreminder_nlufallback_outofscopeinput{{React to nlu fallback<br>and out-of-scope input}}:::action
    nlu_fallback --> action_reaction_to_setreminder_nlufallback_outofscopeinput
    action_reaction_to_setreminder_nlufallback_outofscopeinput --> D1{Decision<br>point}:::decision
    D1 --> resp_out_of_scope(Vielen Dank für Ihre Eingabe.<br>Leider gehört diese Frage aktuell<br>nicht zu meinem Anwendungsbereich.):::response
    D1 --> resp_fallback(Leider verstehe ich diese Eingabe<br>noch nicht. Aktuell zeige ich<br>Ihnen nur exemplarisch ein paar<br>Funktionalitäten und muss noch viel<br>von Ihren Eingaben lernen. Versuchen Sie<br>gerne, Ihr Anliegen nochmal anders<br>zu formulieren.):::response

    %% EXTERNAl_reminder
    S --> EXTERNAL_reminder(External<br>reminder):::intent
    EXTERNAL_reminder --> action_react_to_reminder{{React to reminder}}:::action
    action_react_to_reminder --> resp_reminder("Erinnerung: {reminder note}"):::response

    %% Ask for phone number
    S --> ask_for_phone_number(Ask for phone<br>number):::intent
    ask_for_phone_number --> phone_number_form{{ Enter a phone number form}}:::action
    
    %% forms
    subgraph f
        direction TB
        phone_number_form --> form_questions[[1. First name<br>2. Last name<br>3. Role<br>4. Team]]:::form
        form_questions:::forms -.-> resp_phone_number_form("1. Bitte geben Sie den Vornamen<br>der Person ein, falls<br>bekannt.<br>2. Bitte geben Sie den Nachnamen<br>der Person ein.<br>3. Welche Position hat die Person?<br>(Stationsärztin, ...).<br>4. Bitte geben Sie den Bereich ein,<br>in dem die Person arbeitet!<br>(Endoskopie, Schmerzbereich, Psychosomatik,<br>Gastroenterologie, ...)"):::response
    
        resp_phone_number_form -.-> form_questions
        resp_phone_number_form -.-> D2{Decision<br>point}:::decision
        D2 -.-> resp_phone_number_form
        end

    %% submit
    D2 --> submit{{Submit a phone<br>number form}}:::action
    submit --> resp_submit(Super, vielen Dank für die Eingaben!):::response

    resp_submit --> slots_values_forms{{Slots values for<br>phone number form}}:::action
    slots_values_forms --> resp_slots_values("Künftig können Sie an dieser<br>Stelle eine Liste der für Ihre<br>Eingaben gefundenen Telefonnummern<br>sehen. Folgende Angaben habe ich für<br>die Person, deren Telefonnummer Sie<br>suchen, verstanden:<br>Vorname: {first_name}<br>Nachname: {last_name}<br>Bereich: {team}<br>Position: {role}"):::response

    %% reset phone number form slots
    resp_slots_values --> reset_form_slots{{Reset phone number<br>form slots}}:::action

    %% introduction of chatbot
    S --> request_intro(Request a chatbot<br>introduction):::intent
    request_intro --> utter_intro{{Utter chatbot<br>introduction}}:::action
    utter_intro --> resp_intro("Ich bin DocTalk, ein Chatbot<br>im Forschungsprojekt DocTalk. Im<br>Rahmen des Forschungsprojektes ist<br>es das Ziel, dass ich Ihnen als<br>Prototyp zur Verfügung stehe und<br>Sie auf diese Weise meine<br>Funktionen testen und in Studien<br>erfahren, mitentwickeln und<br>evaluieren können."):::response

    %% scope of application
    resp_intro --> scope_of_application{{Scope of application}}:::action
    scope_of_application --> resp_application("Mittelfristig ist es das Ziel,<br>dass ich prototypisch Assistenzärzt*innen<br>pro-aktiv und transparent in<br>ihren Kommunikations- und nachhaltigen<br>Lernprozessen unterstütze. In dieser<br>ersten Version können Sie mit mir<br>bereits ein paar kleine Funktionalitäten<br>ausprobieren. Aktuell kann ich Ihnen<br>bei folgenden Aufgaben helfen:<br>Informationen zur Krankmeldung,<br>Erinnerung erstellen, Telefonnummer<br>erfragen, Übersicht der<br>Psychosomatik-Pflichttermine."):::response

    %% faq_overview_mandatory_dates_in_psychosomatic_clinic
    S --> overview_psychosomatic(Ask for overview<br>mandatory dates in<br>psychosomatic clinic):::intent
    overview_psychosomatic --> utter_overview_psych{{Utter overview<br>mandatory dates}}:::action
    utter_overview_psych --> resp_overview_psych("Künftig können Sie an dieser<br>Stelle eine Übersicht der Pflichttermine<br>in der Psychosomatik sehen."):::response

    %% diagram styling
    classDef intent fill:#A0DAA9, stroke:#000000;
    classDef action fill:#FDAC53, stroke:#000000;
    classDef decision fill:#98B4D4, stroke:#000000;
    classDef response fill:#FFFFDB, stroke:#FE840E;
    classDef checkpoint fill:#FF6F61, stroke:#000000;
    classDef form fill:	#ffffff, stroke:#98B4D4;
    
    %% styling loop
    classDef graph1 fill:#EEFAE5;
    class f graph1
    %% style for legend
    classDef ls fill:#FEFE94, stroke:#f66,stroke-width:4px;
    class legend ls
```

## Interaction Concept Based on the Chatbot Training Stories

**Legend**

```mermaid
graph LR;
    subgraph Legend
        int(Intent):::intent
        act{{Action}}:::action
        resp(Response):::response
        dec{Decision<br>point}:::decision
        check[\Checkpoint\]:::checkpoint
    end

   %% diagram styling
    classDef intent fill:#A0DAA9, stroke:#000000;
    classDef action fill:#FDAC53, stroke:#000000;
    classDef decision fill:#98B4D4, stroke:#000000;
    classDef response fill:#FFFFDB, stroke:#FE840E;
    classDef checkpoint fill:#FF6F61, stroke:#000000;
    classDef f fill:#ecffb3, stroke:#000000;
    class Legend f

```

**Interaction Concept**

```mermaid
flowchart TB;
    %% Krankschreibung
    S[START] --> krankschreibung(Sick leave):::intent
    style S fill:#939597
    %% intent
    krankschreibung --> utter_art_krankmeldung{{Utter type of health insurance }}:::action
    %% action
    utter_art_krankmeldung --> resp_art_krankmeldung(Zur welcher Art der Krankmeldung<br>brauchen Sie Informationen?):::response
    resp_art_krankmeldung --> d_krank{Decision<br>point}:::decision
    d_krank --> resp_personal(Krankmeldung bei der Personalabteilung):::response
    d_krank --> resp_intern(Interne Krankmeldung):::response
    %% checkpoint
    resp_personal --> check_art_krankmeldung[\Check type of<br>health insurance\]:::checkpoint
    resp_intern --> check_art_krankmeldung

    %% Krankmeldung Personalabteilung
    %% intent
    check_art_krankmeldung --> personal(Human ressources department):::intent
    %% action utter_krank_personalabtelung
    personal --> utter_krank_personal{{Utter illness notification<br>for human ressources<br>department}}:::action
    utter_krank_personal --> resp_krank_personal("Eine AU muss dem GB am<br>4. Tag der Erkrankung vorliegen,<br>bei Erkrankung des Kindes ab<br>dem 1. Tag. Hierbei zählen<br>Kalendertage, d.h. auch das Wochenende<br>wird mitgerechnet. Das Original muss<br>nach Mailversand nicht nachgereicht<br>werden. Bitte halten Sie die<br>AU jedoch mind. 2 Wochen<br>vor, falls es zu Übermittlungsfehlern<br>gekommen sein sollte. Wir empfehlen<br>eine Aufbewahrung bis zu 6 Monaten."):::response
    %% action utter_in_ausbildung
    resp_krank_personal --> utter_in_ausbildung{{Utter in<br>training}}:::action
    utter_in_ausbildung --> resp_frage(Sind Sie derzeit in Ausbildung<br>bei der Charité?):::response
    resp_frage --> d_in_ausbild{Decision<br>point}:::decision
    d_in_ausbild --> resp_yes(Ja):::response
    d_in_ausbild --> resp_no(Nein):::response
    %% checkpoint in_ausbildung
    resp_yes --> checkpoint_in_ausbild[\In training\]:::checkpoint
    resp_no --> checkpoint_in_ausbild

    %%  krankmeldung_interne_krankmeldung
    check_art_krankmeldung --> int_krankmeldung(Internal notification of illness):::intent
    %% action utter_interne_krankmeldung
    int_krankmeldung --> utter_int_krankmeldung{{Utter internal<br>notification of illness}}:::action 
    utter_int_krankmeldung --> resp_int_krank("Im Falle einer Krankheit müssen<br>Sie sich am gleichen Tag kurz<br>telefonisch (Link: Liste der<br>Telefonnummern je Station) auf<br>Station rückmelden (für Ärzte<br>und Psychologen) sowie den die<br>zuständige(n) Oberarzt/Oberärztin, Labor-<br>oder Forschungsleitung per Mail<br>informieren."):::response
    %% decision point
    resp_int_krank --> d_int_krank{Decision<br>point}:::decision
    d_int_krank --> resp_danke(Danke, das reicht mir!):::response
    d_int_krank --> resp_mail_beachten(Was muss ich bei der Mail beachten?):::response

    resp_mail_beachten --> checkp_krankmeldung_int_krankmeldung_mail_info[\Internal notification<br> of illness<br>Mail information\]:::checkpoint
    resp_danke --> checkp_krankmeldung_int_krankmeldung_mail_info

    %% krankmeldung_interne_krankmeldung_mail_info_affirm
    checkp_krankmeldung_int_krankmeldung_mail_info --> mail_beachten(Take mail into account):::intent
    mail_beachten --> utter_krank_mail_anleitung{{Utter illness notification<br>via mail}}:::action
    utter_krank_mail_anleitung --> resp_krank_mail_anleitung("In die Mail nehmen Sie<br>bitte folgende Informationen auf:<br>voraussichtliche Abwesenheit  zu<br>vertretende Termine (bspw. Einzel- oder<br>Gruppentherapien, Lehre, Dienst,<br>Ambulanztermine, etc.). Setzen Sie<br>(Name) (Dienstplanbeauftrage(r)) und<br>Frau M. in cc."):::response
    resp_krank_mail_anleitung --> utterend{{Utter end}}:::action
    utterend --> responseend(Ich hoffe ich konnte Ihnen<br>weiterhelfen!):::response
    
    %% krankmeldung_interne_krankmeldung_end
    checkp_krankmeldung_int_krankmeldung_mail_info --> end_intent(End):::intent
    end_intent --> utterend{{Utter end}}:::action


    %% krankmeldung_personalabteilung_in_ausbildung_affirm
    checkpoint_in_ausbild --> yes_intent(Yes intent):::intent
    yes_intent --> utter_krank{{Utter ill}}:::action
    utter_krank --> resp_krank("Weitere Informationen zur Krankschreibung bei<br>der Personalabteilung finden Sie im Intranet:<br>https://intranet.charite.de/personal/<br>service/verfahrensanweisung_merkblaetter/"):::response


    
    %% krankmeldung_personalabteilung_in_ausbildung_gesetzlich_versichert_affirm
    checkp_gesetzlich_versichert[\Statutory health insurance\]:::checkpoint --> yes_intent2(Yes intent):::intent
    yes_intent2 --> utter_krankmeldung_personalabteilung_anleitung{{Utter illness notification<br>for human ressources<br>department}}:::action
    utter_krankmeldung_personalabteilung_anleitung --> resp_anleitung("Krankmeldung bei der Personalabteilung:<br>- Per Post:Personalnummer auf AU Attest<br>des Kindes schreiben. Brief an<br>die Adresse (Adresse) senden<br>- Per Mail: Photographie (z.B. mit Handy)<br>oder Scan der AU des Attests<br>des Kindes an Mail anhängen<br><br>Betreff: Personalnummer<br>Mail an: krankenschein@charite.de senden"):::response

    %% krankmeldung_personalabteilung_in_ausbildung_deny
    checkpoint_in_ausbild --> no_intent(No intent):::intent 
    no_intent --> utter_art_versicherung{{Utter type of<br>health insurance}}:::action
    utter_art_versicherung --> resp_fragen(Sind Sie gesetzlich versichert?):::response
    resp_fragen --> d_y_n{Decision<br>point}:::decision
    d_y_n --> resp_yy(Ja):::response
    d_y_n --> resp_nn(Nein):::response
    resp_yy --> checkp_gesetzlich_versichert
    resp_nn --> checkp_gesetzlich_versichert


    %% krankmeldung_personalabteilung_in_ausbildung_gesetzlich_versichert_deny
    checkp_gesetzlich_versichert --> no_intent2(No intent):::intent
    no_intent2 --> utter_krank
    resp_krank --> utterend


    %% diagram styling
    classDef intent fill:#A0DAA9, stroke:#000000;
    classDef action fill:#FDAC53, stroke:#000000;
    classDef decision fill:#98B4D4, stroke:#000000;
    classDef response fill:#FFFFDB, stroke:#FE840E;
    classDef checkpoint fill:#FF6F61, stroke:#000000;
    
```

```mermaid
flowchart TB;
    %% Story from Conversation ID 1561e7ed69ad4a1fa91fee7f14d2cbb7, Zeile 87 
    S[START] --> krankschreibung(Sick leave):::intent
    style S fill:#939597
    %% intent
    krankschreibung --> utter_art_krankmeldung{{Utter type of<br>illness notification}}:::action
    %% action
    utter_art_krankmeldung --> resp_art_krankmeldung(Zur welcher Art der Krankmeldung<br>brauchen Sie Informationen?):::response
    resp_art_krankmeldung --> d_krank{Decision<br>point}:::decision
    d_krank --> resp_personal(Krankmeldung bei der Personalabteilung ):::response
    d_krank --> resp_intern(Interne Krankmeldung):::response
    resp_intern --> interne_krankmeldung(Internal notification<br>of illness):::intent
  
    interne_krankmeldung --> utter_interne_krankmeldung{{Utter internal<br>notification of illness}}:::action 
    utter_interne_krankmeldung --> resp_intern_krank("Im Falle einer Krankheit müssen<br>Sie sich am gleichen Tag kurz<br>telefonisch (Link: Liste der<br>Telefonnummern je Station) auf<br>Station rückmelden (für Ärzte<br>und Psychologen) sowie den die<br>zuständige(n) Oberarzt/Oberärztin, Labor-<br>oder Forschungsleitung per Mail<br>informieren."):::response
    resp_intern_krank --> end_intent(End):::intent
    end_intent --> utterend{{Utter end}}:::action
    utterend --> responseend(Ich hoffe ich konnte Ihnen<br>weiterhelfen!):::response


    %% Begrüßung
    S --> hallo_int(Hello):::intent
    hallo_int --> utter_hallo{{Utter hello}}:::action
    utter_hallo --> resp_hallo("Schön, dass Sie vorbeischauen! Wie<br>kann ich Ihnen helfen?"):::response

    %% Verabschiedung
    S --> tschuess_int(Goodbye):::intent
    tschuess_int --> utter_tschuess{{Utter goodbye}}:::action
    utter_tschuess --> resp_tschuess("Vielen Dank für die Unterhaltung!<br>Bis bald!"):::response

    %% Danke&Bitte
    S --> danke_int(Thank you):::intent
    danke_int --> utter_bitte{{Utter you<br>are welcome}}:::action
    utter_bitte --> resp_bitte("Bitte!"):::response

    %% Anwendungsbereich zeigen
    S --> anwendungsber_erfragen__int(Ask for scope of application):::intent
    anwendungsber_erfragen__int --> action_scope_of_application{{Action scope of application}}:::action
    action_scope_of_application --> resp_action_scope_of_application("Mittelfristig ist es das Ziel,<br>dass ich prototypisch Assistenzärzt*innen<br>pro-aktiv und transparent in<br>ihren Kommunikations- und nachhaltigen<br>Lernprozessen unterstütze. In dieser<br>ersten Version können Sie mit mir<br>bereits ein paar kleine Funktionalitäten<br>ausprobieren. Aktuell kann ich Ihnen<br>bei folgenden Aufgaben helfen:<br>Informationen zur Krankmeldung,<br>Erinnerung erstellen, Telefonnummer<br>erfragen, Übersicht der<br>Psychosomatik-Pflichttermine."):::response


    %% diagram styling
    classDef intent fill:#A0DAA9, stroke:#000000;
    classDef action fill:#FDAC53, stroke:#000000;
    classDef decision fill:#98B4D4, stroke:#000000;
    classDef response fill:#FFFFDB, stroke:#FE840E;
    classDef checkpoint fill:#FF6F61, stroke:#000000;
    classDef entity fill:#FFFFDB, stroke:#245c2c;
    classDef noentity fill:#FFFFDB, stroke:#8A0808;

```

**Setting a reminder with different conversational flows**
```mermaid
flowchart TB;
    %% Erinnerung sad change note/ time/ both & Erinnerung sad change note time missing
    S[START] --> erinnerung_setzen_int(Set a reminder):::intent
    style S fill:#939597

    erinnerung_setzen_int --> entity_time(Time slot was set):::entity 
    entity_time --> act_ask_for_reminder_note{{Ask for reminder note}}:::action

    act_ask_for_reminder_note --> resp_ask_reminder_note("Woran soll ich Sie erinnern?<br>Schreiben Sie mir am besten<br>den Inhalt, den die Notiz<br>haben soll."):::response
    resp_ask_reminder_note --> intents(Out of scope input<br>NLU fallback<br>Set a reminder):::intent

    intents --> act_reaction_to_setreminder_nlufallback_outofscopeinput{{Reaction to set reminder,<br>NLU fallback, out of scope<br>input}}:::action
    act_reaction_to_setreminder_nlufallback_outofscopeinput --> d_reaction_to_setreminder{Decision<br>point}:::decision
    d_reaction_to_setreminder --> resp1_reaction("Vielen Dank für Ihre Eingabe.<br>Leider gehört diese Frage aktuell<br>nicht zu meinem Anwendungsbereich."):::response
    d_reaction_to_setreminder --> resp2_reaction("Leider verstehe ich diese Eingabe<br>noch nicht. Aktuell zeige ich<br>Ihnen nur exemplarisch ein paar<br>Funktionalitäten und muss noch viel<br>von Ihren Eingaben lernen. Versuchen Sie<br>gerne, Ihr Anliegen nochmal anders<br>zu formulieren."):::response
    d_reaction_to_setreminder --> resp3_reaction("Potentiellen Hand-through Pfad in action<br>action_reaction_to_setreminder_nlufallback_outofscopeinput<br>aktiviert."):::response
    d_reaction_to_setreminder --> resp4_reaction("Reminder note: user input"):::response
    
    resp4_reaction --> act_utter_confirm_reminder_note{{Utter confirm<br>reminder note}}:::action
    act_utter_confirm_reminder_note --> frage_act_utter_confirm('Möchten Sie, dass ich für<br>Sie folgende Erinnerung erstelle:<br>reminder_note?'):::response
    frage_act_utter_confirm --> resp_confirm_yes("Ja"):::response
    frage_act_utter_confirm --> resp_confirm_no("Nein"):::response

    %% Happy Path
    resp_confirm_yes --Time slot<br>was set--> action_confirm_time{{Confirm time}}:::action
    action_confirm_time --> resp_action_confirm_time("Ich werde Sie am {date}<br>um {time} Uhr erinnern. Ist<br>das in Ordnung?"):::response
    resp_action_confirm_time --> resp_ja("Ja"):::response
    resp_action_confirm_time --> resp_nein("Nein"):::response
    resp_ja --> act_inform_note_time{{Inform note time}}:::action
    act_inform_note_time --> resp_act_inform_note_time("Ich habe eine Erinnerung am<br>{date} um {time} Uhr erstellt.<br>Die Erinnerung enthält die Notiz:<br>{reminder_note}."):::response
    resp_act_inform_note_time --> act_set_reminder{{Set reminder}}:::action
    act_set_reminder --> resp_reminder_set(Reminder is scheduled):::response
    resp_reminder_set --> act_delete_reminder_slots{{Delete reminder slots}}:::action
    act_delete_reminder_slots --> resp_del_reminder_slots(reminder_note and time are deleted):::response

    %% Erinnerung Sad time missing
    erinnerung_setzen_int --> entity_notime(Time slot was not set):::noentity 
    entity_notime --> act_ask_for_reminder_note
    resp_confirm_yes --Time slot was<br>not set--> utter_missing_time{{Utter missing<br>time}}:::action
    utter_missing_time --> resp_missing_time('Wann soll ich Sie erinnern?'):::response
    resp_missing_time --> inform(Inform):::intent
    inform --> time_set(Time slot was set):::entity
    time_set --> action_confirm_time

    
    resp_confirm_no --> act_del_reminder_note_slot{{Delete reminder note slot}}:::action
    act_del_reminder_note_slot --> resp_delete_rem_note(reminder_note is deleted.<br>It can be set again):::response
    resp_delete_rem_note -->  act_ask_for_reminder_note{{Ask for reminder note}}:::action


    %% Erinnerung sad change time
    resp_nein --> act_delete_time_slot{{Delete time slot}}:::action
    act_delete_time_slot --> resp_delete_time_slot(Time slot is deleted):::response
    resp_delete_time_slot --> utter_missing_time




    %% diagram styling
    classDef intent fill:#A0DAA9, stroke:#000000;
    classDef action fill:#FDAC53, stroke:#000000;
    classDef decision fill:#98B4D4, stroke:#000000;
    classDef response fill:#FFFFDB, stroke:#FE840E;
    classDef checkpoint fill:#FF6F61, stroke:#000000;
    classDef entity fill:#FFFFDB, stroke:#245c2c;
    classDef noentity fill:#FFFFDB, stroke:#8A0808;
```

```mermaid
flowchart TB;
    %%  Erinnerung Sad get time in reminder note
    S[START] --> erinnerung_setzen_int(Set a reminder):::intent
    style S fill:#939597

    erinnerung_setzen_int --> act_ask_for_reminder_note{{Ask for reminder note}}:::action
    act_ask_for_reminder_note --> resp_ask_reminder_note("Woran soll ich Sie erinnern?<br>Schreiben Sie mir am besten<br>den Inhalt, den die Notiz<br>haben soll."):::response
    resp_ask_reminder_note --> intents_inform(Inform):::intent
    intents_inform --> intents_inform_entity(Time slot was set):::entity
    intents_inform_entity --> act_reaction_to_setreminder_nlufallback_outofscopeinput{{Reaction to set reminder,<br>NLU fallback, out of scope<br>input}}:::action
 
    act_reaction_to_setreminder_nlufallback_outofscopeinput --> d_reaction_to_setreminder{Decision<br>point}:::decision
    d_reaction_to_setreminder --> resp4_reaction("Reminder note: user input"):::response
    
    resp4_reaction --> act_utter_confirm_reminder_note{{Utter confirm<br>reminder note}}:::action
    act_utter_confirm_reminder_note --> frage_act_utter_confirm('Möchten Sie, dass ich für<br>Sie folgende Erinnerung erstelle:<br>reminder_note?'):::response
    frage_act_utter_confirm --> resp_confirm_yes("Ja"):::response
    frage_act_utter_confirm --> resp_confirm_no("Nein"):::response


    resp_confirm_yes --> action_confirm_time{{Confirm time}}:::action
    action_confirm_time --> resp_action_confirm_time("Ich werde Sie am {date}<br>um {time} Uhr erinnern. Ist<br>das in Ordnung?"):::response
    resp_action_confirm_time --> resp_ja("Ja"):::response
    resp_action_confirm_time --> resp_nein("Nein"):::response
    resp_ja --> act_inform_note_time{{Inform note time}}:::action
    act_inform_note_time --> resp_act_inform_note_time("Ich habe eine Erinnerung am<br>{date} um {time} Uhr erstellt.<br>Die Erinnerung enthält die Notiz:<br>{reminder_note}."):::response
    resp_act_inform_note_time --> act_set_reminder{{Set reminder}}:::action
    act_set_reminder --> resp_reminder_set(Reminder is scheduled):::response
    resp_reminder_set --> act_delete_reminder_slots{{Delete reminder slots}}:::action
    act_delete_reminder_slots --> resp_del_reminder_slots(reminder_note and time are deleted):::response


    %% diagram styling
    classDef intent fill:#A0DAA9, stroke:#000000;
    classDef action fill:#FDAC53, stroke:#000000;
    classDef decision fill:#98B4D4, stroke:#000000;
    classDef response fill:#FFFFDB, stroke:#FE840E;
    classDef checkpoint fill:#FF6F61, stroke:#000000;
    classDef entity fill:#FFFFDB, stroke:#245c2c;
    classDef noentity fill:#FFFFDB, stroke:#8A0808;
```

**Another example of conversation on phone number request**
```mermaid
flowchart TB;
    %% FAQ ask for phone number 1,2,3 
    S[START] --> ask_for_phone_number(Ask for phone<br>number):::intent
    style S fill:#939597
    S --> anwendungsbereich_erfragen(Ask for scope of application):::intent
    anwendungsbereich_erfragen --> act_scope_of_application{{Action scope of application}}:::action
    act_scope_of_application --> resp_application("Mittelfristig ist es das Ziel,<br>dass ich prototypisch Assistenzärzt*innen<br>pro-aktiv und transparent in<br>ihren Kommunikations- und nachhaltigen<br>Lernprozessen unterstütze. In dieser<br>ersten Version können Sie mit mir<br>bereits ein paar kleine Funktionalitäten<br>ausprobieren. Aktuell kann ich Ihnen<br>bei folgenden Aufgaben helfen:<br>Informationen zur Krankmeldung,<br>Erinnerung erstellen, Telefonnummer<br>erfragen, Übersicht der<br>Psychosomatik-Pflichttermine."):::response
    resp_application --> ask_for_phone_number
    ask_for_phone_number --> first_name(First name is given):::entity
    ask_for_phone_number --> last_name(Last name is given):::entity
    first_name --> phone_number_form{{ Enter a phone number form}}:::action
    last_name --> phone_number_form
    

    %% forms
    subgraph Form
        direction TB
        phone_number_form --> form_questions[[1. First name<br>2. Last name<br>3. Role<br>4. Team]]:::form
        form_questions:::forms -.-> resp_phone_number_form("1. Bitte geben Sie den Vornamen<br>der Person ein, falls<br>bekannt.<br>2. Bitte geben Sie den Nachnamen<br>der Person ein.<br>3. Welche Position hat die Person?<br>(Stationsärztin, ...).<br>4. Bitte geben Sie den Bereich ein,<br>in dem die Person arbeitet!<br>(Endoskopie, Schmerzbereich, Psychosomatik,<br>Gastroenterologie, ...)"):::response
    
        resp_phone_number_form -.-> form_questions
        resp_phone_number_form -.-> D2{Decision<br>point}:::decision
        D2 -.-> resp_phone_number_form
        end

    %% submit
    D2 --> submit{{Submit a phone<br>number form}}:::action
    submit --> resp_submit(Super, vielen Dank für die Eingaben!):::response

    resp_submit --> slots_values_forms{{Slots values for<br>phone number form}}:::action
    slots_values_forms --> resp_slots_values("Künftig können Sie an dieser<br>Stelle eine Liste der für Ihre<br>Eingaben gefundenen Telefonnummern<br>sehen. Folgende Angaben habe ich für<br>die Person, deren Telefonnummer Sie<br>suchen, verstanden:<br>Vorname: {first_name}<br>Nachname: {last_name}<br>Bereich: {team}<br>Position: {role}"):::response

    %% reset phone number form slots
    resp_slots_values --> reset_form_slots{{Reset phone number<br>form slots}}:::action
    reset_form_slots --> resp_reset_form_slots(Reset all phone number form<br>slots: team, first_name,<br>last_name, role):::response

    
    %% styling loop
    classDef graph1 fill:#EEFAE5;
    class Form graph1
    %% style for legend
    classDef ls fill:#FEFE94, stroke:#f66,stroke-width:4px;
    class legend ls

    %% diagram styling
    classDef intent fill:#A0DAA9, stroke:#000000;
    classDef action fill:#FDAC53, stroke:#000000;
    classDef decision fill:#98B4D4, stroke:#000000;
    classDef response fill:#FFFFDB, stroke:#FE840E;
    classDef checkpoint fill:#FF6F61, stroke:#000000;
    classDef entity fill:#FFFFDB, stroke:#245c2c;
    classDef noentity fill:#FFFFDB, stroke:#8A0808;
    classDef form fill:	#ffffff, stroke:#98B4D4;
