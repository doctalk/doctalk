# This file differs from the original chatbot code as derived through initializing a chatbot based on Rasa 2.0.2 (https://github.com/RasaHQ/rasa/releases/tag/2.0.2).

# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions



import datetime
from dateutil.parser import parse
from pytz import timezone           # required for default times using Berlin timezone


from rasa_sdk.events import FollowupAction, ReminderScheduled, SlotSet, SessionStarted, ActionExecuted, EventType, ConversationPaused, ConversationResumed
from typing import Any, Text, Dict, List, Union
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction
import json
import os
import requests
import urllib3


# Welcome Message

class ActionUtterConditionalWelcomeMessage(Action):
    """Welcomes the user based on whether the user is new to DocTalk (< add contition here >) or not
    (< add condition here >)"""

    def name(self) -> Text:
        return "action_utter_conditional_welcome_message"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker, domain: Dict[Text, Any]) -> List[EventType]:

        events = []

        # check current value of user_is_new
        user_is_new = tracker.get_slot("user_is_new")

        if user_is_new:
            # recognize that the user is not new after this first interaction
            events.append(SlotSet('user_is_new', False))

            # enforce follow-up action: action_utter_welcome_message_new_user
            events.append(FollowupAction('action_utter_welcome_message_new_user'))

        else:
            # enforce follow-up action: action_utter_welcome_message_returning_user
            events.append(FollowupAction('action_utter_welcome_message_returning_user'))

        return events


# Reminder Actions

class ActionAskForReminderNote(Action):
    """Asks for reminder note"""

    def name(self) -> Text:
        return "action_ask_for_reminder_note"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker, domain: Dict[Text, Any]) -> List[EventType]:
        last_intent = tracker.latest_message['intent'].get('name')

        # check current value of reminder_conversation
        reminder_conversation = tracker.get_slot("reminder_conversation")

        # set indicator for reminder conversations to True, if the last intent was "erinnerung_setzen"
        if not reminder_conversation:
            reminder_conversation = last_intent == "request_setting_a_reminder"

        message = "Woran soll ich Sie erinnern? Schreiben Sie mir am besten den Inhalt, den die Notiz haben soll."
        dispatcher.utter_message(text=message,)

        return [SlotSet('reminder_conversation', reminder_conversation)]

"""class ActionAskForAmbossQuery(Action):
    #Asks for reminder note

    def name(self) -> Text:
        return "action_ask_for_amboss_query"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker, domain: Dict[Text, Any]) -> List[EventType]:
        events=[]
        last_intent = tracker.latest_message['intent'].get('name')

        # check current value of reminder_conversation
        ambossAPI_conversation = tracker.get_slot("ambossAPI_conversation")


        message = "Hi, ich bin bei action_ask_for_amboss_query"
        dispatcher.utter_message(text=message, )

        # set indicator for reminder conversations to True, if the last intent was "erinnerung_setzen"
        if not ambossAPI_conversation:
            ambossAPI_conversation = last_intent == "amboss"
            print()

        message = "Welches Thema soll ich in Amboss nachschalgen? Schreiben Sie mir am besten den Inhalt"
        dispatcher.utter_message(text=message, )


        # enforce next action action: utter_confirm_reminder_note
        events.append(FollowupAction('action_amboss_api_call'))


        return [SlotSet('ambossAPI_conversation', ambossAPI_conversation)]

class ActionReactionToAmbossQueryNlufallbackOutofscopeinput(Action):
    #Asks Amboss Query

    def name(self) -> Text:
        return "action_reaction_to_amboss_query_nlufallback_outofscopeinput"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[EventType]:

        events = []
        message = ""

        # check whether the bot currently runs a reminder conversation and waits for free text input

        ambossAPI_conversation = tracker.get_slot("ambossAPI_conversation")


        if ambossAPI_conversation:
            message = "This is action_reaction_to_amboss_query_nlufallback_outofscopeinput"
            dispatcher.utter_message(text=message, )

            # reset information: the bot does no longer wait for a free text input as a reminder_note
            events.append(SlotSet('ambossAPI_conversation', False))

            # set reminder_note and add to events
            #reminder_note = tracker.latest_message["text"]
            amboss_search_term= tracker.latest_message["text"]
            events.append(SlotSet('ambossAPI_conversation', amboss_search_term))

            # enforce next action action: utter_confirm_reminder_note
            #events.append(FollowupAction('utter_confirm_reminder_note'))
            events.append(FollowupAction('action_amboss_api_call'))
        else:
            last_intent = tracker.latest_message['intent'].get('name')
            if last_intent == "out_of_scope_eingabe":
                message = "Vielen Dank für Ihre Eingabe. Leider gehört diese Frage aktuell nicht zu meinem Anwendungsbereich."
                dispatcher.utter_message(text=message, )
                events.append(FollowupAction('action_scope_of_application'))
            elif last_intent == "nlu_fallback":
                message = "Leider verstehe ich diese Eingabe noch nicht. Aktuell zeige ich Ihnen nur exemplarisch ein paar Funktionalitäten und muss noch viel von Ihren Eingaben lernen.\nVersuchen Sie gerne, Ihr Anliegen nochmal anders zu formulieren."
                dispatcher.utter_message(text=message, )
                events.append(FollowupAction('action_listen'))
            else:
                message = "Potentiellen Hand-through Pfad in action action_reaction_to_setreminder_nlufallback_outofscopeinput aktiviert."
                dispatcher.utter_message(text=message, )
                events.append(FollowupAction('action_listen'))

        return events"""


class ActionReactionToSetreminderNlufallbackOutofscopeinput(Action):
    """Reacts to all texts that cannot be classified well:
    a) Sets reminder_note during reminder conversation,
    b) or else: Reacts to out-of-scope user input,
    c) or else: Reacts to unknown user input"""

    def name(self) -> Text:
        return "action_reaction_to_setreminder_nlufallback_outofscopeinput"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker, domain: Dict[Text, Any]) -> List[EventType]:
        events = []
        message = ""

        # check whether the bot currently runs a reminder conversation and waits for free text input
        reminder_conversation = tracker.get_slot("reminder_conversation")

        if reminder_conversation:
            message = "This is reminder_conversation"
            dispatcher.utter_message(text=message, )

            # reset information: the bot does no longer wait for a free text input as a reminder_note
            events.append(SlotSet('reminder_conversation', False))

            # set reminder_note and add to events
            reminder_note = tracker.latest_message["text"]
            events.append(SlotSet('reminder_note', reminder_note))

            # enforce next action action: utter_request_to_confirm_reminder_note
            events.append(FollowupAction('utter_request_to_confirm_reminder_note'))
        else:
            last_intent = tracker.latest_message['intent'].get('name')
            if last_intent == "out_of_scope_input":
                message = "Vielen Dank für Ihre Eingabe. Leider gehört diese Frage aktuell nicht zu meinem Anwendungsbereich."
                dispatcher.utter_message(text=message,)
                events.append(FollowupAction('action_scope_of_application'))
            elif last_intent == "nlu_fallback":
                message = "Leider verstehe ich diese Eingabe noch nicht. Aktuell zeige ich Ihnen nur exemplarisch ein paar Funktionalitäten und muss noch viel von Ihren Eingaben lernen.\nVersuchen Sie gerne, Ihr Anliegen nochmal anders zu formulieren."
                dispatcher.utter_message(text=message,)
                events.append(FollowupAction('action_listen'))
            else:
                message = "Potentiellen Hand-through Pfad in action action_reaction_to_setreminder_nlufallback_outofscopeinput aktiviert."
                dispatcher.utter_message(text=message,)
                events.append(FollowupAction('action_listen'))

        return events


class ActionDeleteReminderNoteSlot(Action):
    """Deletes reminder_note value and sets reminder_conversation to True, so that the reminder_note can be set again"""

    def name(self):
        return "action_delete_reminder_note_slot"

    def run(self, dispatcher, tracker, domain):
        events = []

        # Set reminder conversation to true (setting reminder_note failed before, conversation must continue)
        events.append(SlotSet('reminder_conversation', True))

        # Delete reminder note
        events.append(SlotSet('reminder_note', None))

        return events


class ActionParseConfirmTime(Action):
    def name(self) -> Text:
        return "action_confirm_time"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        notification_date_time_obj = parse(tracker.get_slot("time"))
        human_readable_date = notification_date_time_obj.strftime("%d.%m.%Y,%H:%M ") # convert datetime string
        date = human_readable_date.split(",")[0] # take first element date
        time = human_readable_date.split(",")[1] # take second element time
        message = "Ich werde Sie am {date} um {time} Uhr erinnern. Ist das in Ordnung?".format(date=date, time= time)
        buttons = [
            {
                "title": "Ja",
                "payload":"/yes_intent"
            },
            {
                "title": "Nein",
                "payload":"/no_intent"
            }
        ]
        dispatcher.utter_message(text=message,buttons=buttons)
        return []


class ActionDeleteTimeSlot(Action):
    """Deletes time slot"""

    def name(self):
        return "action_delete_time_slot"

    def run(self, dispatcher, tracker, domain):
        return [SlotSet('time', None)]


class ActionResetReminderSlots(Action):
    """Deletes reminder slots reminder_note and time"""

    def name(self):
        return "action_delete_reminder_slots"

    def run(self, dispatcher, tracker, domain):
        # AllSlotsReset() for all Slots
        return [SlotSet('reminder_note', None), SlotSet('time', None)]


class ActionInformNoteTime(Action):
    """Informs about reminder_note and time of a reminder that will be scheduled during the followup action
    action_set_reminder"""

    def name(self) -> Text:
        return "action_inform_note_time"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        notification_date_time_obj = parse(tracker.get_slot("time"))
        human_readable_date = notification_date_time_obj.strftime("%d.%m.%Y,%H:%M") # convert datetime string
        date = human_readable_date.split(",")[0] # take first element date
        time = human_readable_date.split(",")[1] # take second element time
        reminder_note = tracker.get_slot("reminder_note")
        message = "Ich habe eine Erinnerung am {date} um {time} Uhr erstellt." \
                  " Die Erinnerung enthält die Notiz: " \
                  "{reminder_note}.".format(date=date, time= time,reminder_note=reminder_note)
        dispatcher.utter_message(text=message,)
        return [FollowupAction('action_set_reminder')]


class ActionSetReminder(Action):
    """Schedules a reminder, supplied with the entity values of reminder_note and time."""

    def name(self) -> Text:
        return "action_set_reminder"

    async def run(
            self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:

        # For testing purposes, one can use a default reminder time (or request to schedule the timer two minutes later):
        # 1) Set default reminder time:
        # timezone_Berlin = timezone("Europe/Berlin")
        # date = datetime.datetime.now(timezone_Berlin) + datetime.timedelta(seconds=30)
        # 2) Inform about reminder time:
        # message = "Ich werde Sie in 30 s erinnern."
        # dispatcher.utter_message(text=message,)
        # 3) Show the time at which the reminder will appear:
        # message = date.strftime("%d/%m/%Y, %H:%M, %z, %Z")
        # dispatcher.utter_message(text=message,)

        # Set correct reminder time:
        notification_date_time_obj = parse(tracker.get_slot("time"))
        # For testing: Show the time at which the reminder will appear:
        # message = notification_date_time_obj.strftime("%d/%m/%Y, %H:%M, %z, %Z")
        # dispatcher.utter_message(text=message, )

        # Build reminder entities:
        entities = {
            "reminder_note": tracker.get_slot("reminder_note"),
            "time": notification_date_time_obj.strftime("%d/%m/%Y, %H:%M:%S")
        }

        reminder = ReminderScheduled(
            "EXTERNAL_reminder",
            # trigger_date_time=date,                      # a) use default reminder time
            trigger_date_time=notification_date_time_obj,  # b) use correct reminder time
            entities=entities,
            name="my_reminder" + str(datetime.datetime.now()),
            kill_on_user_message=False,
        )

        return [reminder]


class ActionReactToReminder(Action):
    """Reminder action that is played as an answer to the external (user-intent-like) EXTERNAL_reminder."""

    def name(self) -> Text:
        return "action_react_to_reminder"

    async def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict[Text, Any]]:


        # Extract the reminder entities from the user-message-like received reminder call EXTERNAL_reminder
        # = Extract the entities reminder_note (and time) from "last message" (here: EXTERNAL_reminder):
        reminder_note = tracker.latest_message.get('entities')[0].get('value')
        #time = tracker.latest_message.get('entities')[1].get('value')      # for testing purposes only

        # Utter reminder_note
        message = f"Erinnerung: {reminder_note}"
        dispatcher.utter_message(text=message,)
        return []


# Other custom actions:


class ActionSessionStart(Action):
    """Resets all slots during session start"""

    def name(self) -> Text:
        """This is the name to be mentioned in domain.yml and stories.md files
        for this action. The default behavior of the session start action is to take all existing slots and to carry
        them over into the next session. This custom action overwrites the default session start action."""
        return "action_session_start"

    @staticmethod
    def fetch_slots(tracker: Tracker) -> List[EventType]:
        """Collect slots that contain the user's name and phone number."""

        slots = []
        for key in (["user_is_new"]):
            value = tracker.get_slot(key)
            slots.append(SlotSet(key=key, value=value))
        return slots

    async def run(self, dispatcher: CollectingDispatcher, tracker, domain: Dict[Text, Any], ) -> List[EventType]:
        """This run function will be executed when "action_session_start" is
            triggered. Make sure the slots are available at start in Rasa x so that they can be used."""

        # the session should begin with a `session_started` event
        events = [SessionStarted()]

        # any slots that should be carried over should come after the
        # `session_started` event
        events.extend(self.fetch_slots(tracker))

        # enforce next action action: action_listen
        events.append(ActionExecuted("action_listen"))

        return events


class ActionScopeOfApplication(Action):
    def name(self) -> Text:
        return "action_scope_of_application"

    def run(self, dispatcher: CollectingDispatcher,tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # Opening JSON file
        actions_folder = os.path.dirname(os.path.abspath(__file__))
        scope_json = os.path.join(actions_folder, 'scope.json')
        f = open(scope_json, )
        # returns JSON object as a dictionary
        data = json.load(f)
        scope_list = data.get("scopeOfApplication")
        buttons = []
        for elem in scope_list:
            temp_buttons = {"title":elem.get("title"),"payload": elem.get("intent")}
            buttons.append(temp_buttons)
        # Closing file
        f.close()
        message = "Mittelfristig ist es das Ziel, dass ich prototypisch Assistenzärzt*innen pro-aktiv und transparent in ihren Kommunikations- und nachhaltigen Lernprozessen unterstütze.\nIn dieser ersten Version können Sie mit mir bereits ein paar kleine Funktionalitäten ausprobieren.\nAktuell kann ich Ihnen bei folgenden Aufgaben helfen:"
        buttons = buttons
        dispatcher.utter_message(text=message, buttons=buttons)

        return []


class ActionResetPhoneNumberFormSlots(Action):
    """Resets all phone number form slots: team, first_name, last_name, role"""

    def name(self):
        return "action_reset_phone_number_form_slots"

    def run(self, dispatcher, tracker, domain):
        events = []

        # Delete phone number form slots
        events.append(SlotSet('team', None))
        events.append(SlotSet('first_name', None))
        events.append(SlotSet('last_name', None))
        events.append(SlotSet('role', None))

        return events


class ActionAmbossAPICall(Action):
    """Connects to Amboss API and performs a search query"""

    def name(self) -> Text:
        return "action_amboss_api_call"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        buttons = []
        titel = []
        subtitel = []
        bodies = []
        auswahlbuttons = []

        authorization_token= ''

        if authorization_token=='':
            no_token = "Ihre Anfrage ist derzeit nicht möglich. Die Anfrage wurde der externen Quelle Ambos zugeordnet. In dieser Version wurde der hierfür notwendige Ambos-Access-Token entfernt. Bitte fügen sie einen neuen Token hinzu oder ändern die API zu einer anderen externen Quelle."
            dispatcher.utter_message(no_token)
            return


        urllib3.disable_warnings()

        headers = {
            'accept': 'application/json',
            'Authorization': authorization_token,
        }

        # Defines the number of articles that are displayed:
        limit_num = 3

        # Takes the last user input and uses it as the initial amboss_query text:
        topic=tracker.get_slot("amboss_query")

        if topic==None:
            topic = tracker.get_slot("amboss_query2")
            #topic = tracker.latest_message["text"]

        #preprocessing Article Or SOP Serach

        #SOP search
        # in case sop is still part of the query
        if ("SOP" in topic.upper() or "- AMBOSS-SOP" in topic or "VORGEHEN" in topic.upper() or "STANDARD OPERATING PROCEDURE" in topic.upper()):
            topic = topic.replace(" sop", "").replace(" SOP", "").replace("sop ", "").replace("SOP ", "").replace(
                " - AMBOSS-SOP", "").replace(" vorgehen", "").replace("vorgehen ", "").replace(" Vorgehen", "").replace(
                "Vorgehen ", "").replace(" - amboss-sop", "").replace(" amboss-sop", "").replace(" Amboss-sop",
                                                                                                 "").replace(
                " amboss-SOP", "").replace(" Amboss-SOP", "").replace("amboss-sop ", "").replace("Amboss-sop ",
                                                                                                 "").replace(
                "amboss-SOP ", "").replace("Amboss-SOP ", "").replace("standard operating procedures ", "").replace(
                " standard operating procedures", "")

            topic = topic + " - AMBOSS-SOP"

        #Article Search
        #Deletes "amboss" phrases in various spellings:
        else:
            topic = topic.replace('amboss', '')
            topic = topic.replace('aboss', '')
            topic = topic.replace('ambos', '')
            topic = topic.replace('amoss', '')
            topic = topic.replace('mboss', '')
            topic = topic.replace('AMBOSS', '')
            topic = topic.replace('Amboss', '')

        # Sets Amboss API parameters in the requested format by the Amboss API:
        params = (
            ('query', topic),  # 'meningitis antibiotika dosierung'
            ('limit', str(limit_num)),
        )

        # Conducts Amboss API query:
        try:
            response = requests.get('https://symbiontapi.de.staging.labamboss.com/v0/search', headers=headers,
                                params=params, verify=False)
                                # old url: https://symbiontapi.de.staging.labamboss.com/v0_unstable/search
                                # new url: https://symbiontapi.de.staging.labamboss.com/v0/search
        # Note: We do not know, whether the "verify" parameter could be useful for us, yet.

        except (requests.ConnectionError, requests.Timeout) as exception:
            no_conextion_text="Wir haben versucht, mit Ihren Angaben eine Suche in Amboss durchzuführen. Leider ist Amboss aktuell nicht erreichbar. Bitte versuchen Sie es zu einem späteren Zeitpunkt erneut."
            dispatcher.utter_message(no_conextion_text)
            return

        # Textaufbereitung, <b> steht dafür, dass der Text dazwischen bold ist
        str_text = response.text.replace('<b>', '**').replace('</b>', '**').replace('<i>', '').replace('</i>', '')

        data = json.loads(str_text)

        #case no results found
        if (data["pagination"]["totalCount"] == 0):
            dispatcher.utter_message(text= "_Leider gibt es bzgl. **"+topic+"** in Amboss keine Einträge.\nMöchtest du sie vielleicht noch einmal anders stellen, bzw. ist alles richtig geschrieben?_")

            return []

        # Listen müssen für die nächste Suche wieder leer sein

        buttons.clear()
        titel.clear()
        subtitel.clear()
        bodies.clear()
        auswahlbuttons.clear()

        for result in data['results']:
            if ("AMBOSS-SOP" in topic):
                name = result["title"]
                if (topic.upper() in name.upper()):
                    # Listen nochmal sicherheitshalber leeren (wahrscheinlich unnötig, da es wenn dann das erste Ergebnis ist
                    buttons.clear()
                    titel.clear()
                    subtitel.clear()
                    bodies.clear()

                    tabelle = False

                    ueberschrift = result["title"].replace('**', '')
                    titel.append("[**" + result["title"].replace('**', '') + "**](" + result["link"] + ")")
                    if (result["subtitle"] != None):
                        buttons.append(
                            ueberschrift.replace('(', '[').replace(')', ']') + ": " + result["subtitle"].replace('**',
                                                                                                                 '').replace(
                                '(', '[').replace(')', ']'))
                        subtitel.append("**" + result["subtitle"].replace('**', '') + "**")
                    else:
                        buttons.append(ueberschrift.replace('(', '[').replace(')', ']'))
                        subtitel.append("")
                    if (result["body"] != None):
                        body = ""
                        kl_tab = "   "
                        gr_tab = "     "
                        for char in result["body"]:
                            leng = len(body)
                            if (char == "∙"):
                                char = "\n" + char
                            if (char == "◦"):
                                char = "\n" + kl_tab + char
                            if (char == "▪"):
                                char = "\n" + gr_tab + char
                            if (body[leng - 3:leng] == "..."):
                                char = "\n"
                            if (body[leng - 1:leng] == "→"):
                                body = body[0:leng - 2]
                                char = ":" + "\n"
                            if (char == "|"):
                                tabelle = True
                            body += char
                        if (tabelle):
                            body += "\n\n_In diesem Artikel sind die Inhalte vermutlich tabelarisch aufbereitet.\nFalls er für dich interessant wirkt, würde ich dir empfehlen über den Link im Titel die Amboss-Seite direkt zu besuchen._"
                        bodies.append(body)

                else:
                    tabelle = False
                    if (result["type"] == "knowledge_snippet"):
                        ueberschrift = result["title"].replace('**', '')
                        titel.append("**" + result["title"].replace('**', '') + "**")
                    else:
                        ueberschrift = result["title"].replace('**', '')
                        titel.append("[**" + result["title"].replace('**', '') + "**](" + result["link"] + ")")
                    if (result["subtitle"] != None):
                        buttons.append(ueberschrift + ": " + result["subtitle"].replace('**', ''))
                        subtitel.append("**" + result["subtitle"].replace('**', '') + "**")
                    else:
                        if (result["type"] == "knowledge_snippet"):
                            buttons.append("Amboss-Definition: " + ueberschrift)
                        else:
                            buttons.append(ueberschrift)
                        subtitel.append("")
                    if (result["body"] != None):
                        body = ""
                        kl_tab = "   "
                        gr_tab = "     "
                        for char in result["body"]:
                            leng = len(body)
                            if (char == "∙"):
                                char = "\n" + char
                            if (char == "◦"):
                                char = "\n" + kl_tab + char
                            if (char == "▪"):
                                char = "\n" + gr_tab + char
                            if (body[leng - 3:leng] == "..."):
                                char = "\n"
                            if (body[leng - 1:leng] == "→"):
                                body = body[0:leng - 2]
                                char = ":" + "\n"
                            if (char == "|"):
                                tabelle = True
                            body += char
                        if (tabelle):
                            body += "\n\n_In diesem Artikel sind die Inhalte vermutlich tabelarisch aufbereitet.\nFalls er für dich interessant wirkt, würde ich dir empfehlen über den Link im Titel die Amboss-Seite direkt zu besuchen._"
                        bodies.append(body)

            else:
                tabelle = False
                if (result["link"] == None):
                    ueberschrift = result["title"].replace('**', '')
                    titel.append("**" + result["title"].replace('**', '') + "**")
                else:
                    ueberschrift = result["title"].replace('**', '')
                    titel.append("[**" + result["title"].replace('**', '') + "**](" + result["link"] + ")")
                if (result["subtitle"] != None):
                    # wenn (...) in den buttons auftaucht hält rasa das für eine entity -> müssen zu [...] geändert werden
                    buttons.append(
                        ueberschrift.replace('(', '[').replace(')', ']') + ": " + result["subtitle"].replace('**',
                                                                                                             '').replace(
                            '(', '[').replace(')', ']'))
                    subtitel.append("**" + result["subtitle"].replace('**', '') + "**")
                else:
                    if (result["type"] == "knowledge_snippet"):
                        buttons.append("Amboss-Definition: " + ueberschrift.replace('(', '[').replace(')', ']'))
                    else:
                        buttons.append(ueberschrift.replace('(', '[').replace(')', ']'))
                    subtitel.append("")
                if (result["body"] != None):
                    body = ""
                    kl_tab = "   "
                    gr_tab = "     "
                    for char in result["body"]:
                        leng = len(body)
                        if (char == "∙"):
                            char = "\n" + char
                        if (char == "◦"):
                            char = "\n" + kl_tab + char
                        if (char == "▪"):
                            char = "\n" + gr_tab + char
                        if (body[leng - 3:leng] == "..."):
                            char = "\n"
                        if (body[leng - 1:leng] == "→"):
                            body = body[0:leng - 2]
                            char = ":" + "\n"
                        if (char == "|"):
                            tabelle = True
                        body += char
                    if (tabelle):
                        body += "\n\n_In diesem Artikel sind die Inhalte vermutlich tabelarisch aufbereitet.\nFalls er für dich interessant wirkt, würde ich dir empfehlen über den Link im Titel die Amboss-Seite direkt zu besuchen._"
                    bodies.append(body)

        anzahl = len(bodies)

        if (anzahl == 1):
            dispatcher.utter_message(text="_Oooh, du scheinst was ganz besonderes zu suchen. Ich konnte nur einen passenden Amboss-Eintrag zu **" + topic + "** finden._")
        elif  (anzahl == 2):
            dispatcher.utter_message(text="_Oooh, du scheinst was ganz besonderes zu suchen. Ich konnte nur zwei passende Amboss-Einträge zu **" + topic + "** finden._")

        warn_sop_message=0
        for elem in range (0,len(bodies)):
            if (subtitel[0] == ""):
                # in case a fitting SOP could no be found, amboss returns default SOPs. Alert the user about it
                if ("AMBOSS-SOP" in topic) and any(
                        elem_x in titel[elem] for elem_x in ["Kopfschmerzen", "Meningitisches Syndrom", "Schwindel"]) and any(
                        elem_x not in topic for elem_x in ["Kopfschmerzen", "Meningitisches Syndrom", "Schwindel"]) and warn_sop_message==0:
                    dispatcher.utter_message("Leider hat Amboss keine passende SOP zu deinem Suchterm. Aber villeicht werden dir diese SOP helfen.")
                    dispatcher.utter_message("\n")
                    warn_sop_message=1

                dispatcher.utter_message("Titel "+titel[elem] + "\n"+ "Artikel "+ str(elem+1)+":"+"\n" + ">" + bodies[elem]+ "\n") #das ist das ersteg Ergebnis, weite
                dispatcher.utter_message("\n")


        else:
                dispatcher.utter_message("Titel "+titel[elem] + "\n" + "Untertitel " + subtitel[elem] + "\n"+ "Artikel "+ str(elem)+":"+"\n" + ">" + bodies[elem]+ "\n") #Freie Zeile unter Ergebnisssen
                dispatcher.utter_message("\n")

        if ("AMBOSS-SOP" in topic):
            #slot setting for the mode swich button
            topic = topic.replace(" - AMBOSS-SOP", '')
            return [SlotSet("amboss_query", topic),SlotSet("amboss_mode", "Suche auf alle Artikel erweitern.")]
        else:
            topic = topic + " - AMBOSS-SOP"
            return [SlotSet("amboss_query", topic),SlotSet("amboss_mode", "Suche auf SOPs eingrenzen.")]

class SetAmbossSearchTerm(Action):
    def name(self) -> Text:
        return "set_amboss_search_term" #action_amboss_api_call"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        topic = tracker.latest_message["text"]
        return [SlotSet("amboss_query2", topic)]

class FormAmbossCall(FormAction):
    """This amboss form is used only if the first exchange between the user and the bot
    did not lead to a successfull amboss call, and the user answered with a negative feedback,
    i.e., "I want to conduct a query, but with a different query phrase.".
    Then this form is opened and closed if the amboss_query is set."""

    def name(self):
        return "action_amboss_call_negative_feedback_form"

    # We define that the amboss_form requires the slot "amboss_query". This...
    # 1. ... defines that requested_slot = amboss_query
    # 2. ... defines that the amboss_form is left only when the slot amboss_query is filled. (= 1.)
    @staticmethod
    def required_slots(tracker):
        return ["amboss_query"]


    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            "amboss_query": [
                # List of intents that can be said by the user and that will lead to filling
                # the slot amboss_query with the user utterance:
                self.from_text(intent="inform"), #why this intent, see wiki for amboss api
                self.from_text(intent="amboss"),
                self.from_text(intent="out_of_scope_input"),
                self.from_text(intent="nlu_fallback"),
                self.from_text(intent="action_reaction_to_setreminder_nlufallback_outofscopeinput"),
                self.from_text(intent="ask_for_overview_mandatory_dates_in_psychosomatic_clinic"),
                self.from_text(intent="internal_sick_note"),
                self.from_text(intent="ask_for_sick_note_instructions"),
                self.from_text(intent="request_setting_a_reminder"),
                self.from_text(intent="hello"),
                self.from_text(intent="no_intent"),
                self.from_text(intent="yes_intent"),
                self.from_text(intent="faq"),
                self.from_text(intent="thank_you")
            ],
        }

    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:

        return []


class ActionResetAmbossQueryForm(Action):
    """Resets amboss_query Slot"""

    def name(self):
        return "action_reset_amboss_query_slot"

    def run(self, dispatcher, tracker, domain):

        return [SlotSet('amboss_query', None)]


class ActionUtterWelcomeMessageNewUser(Action):
    """Welcomes a new user"""

    def name(self):
        return "action_utter_welcome_message_new_user"

    def run(self, dispatcher, tracker, domain):
        # Build messages:

        message1 = "Hallo, schön, dass Sie vorbeischauen!"
        message2 = "Ich bin DocTalk. Ich möchte Sie in Ihrem klinischen Lernen unterstützen und kann Ihnen bei einigen organisatorischen und medizinischen Fragen und klinischen Prozessen weiterhelfen."
        message3 = "Stellen Sie mir gerne eine Frage!"

        buttons = [
            {
                "title": "Was kannst du?",
                "payload": "/ask_for_scope_of_application"
            },
            {
                "title": "Erzähl mir mehr von dir!",
                "payload": "/request_introduction_of_chatbot"
            },
            # not yet implemented:
            # {
            #    "title": "Erzähl mir mehr über das Projekt DocTalk!",
            #    "payload": "/<intent>"
            # },
            {
                "title": "Wie kann ich den Chatbot bedienen?",
                "payload": "Wie kann ich dich bedienen?"  # intent: faq/ask_chatbot_manual
            }
        ]

        # Utter messages:
        dispatcher.utter_message(text=message1, )
        dispatcher.utter_message(text=message2, )
        dispatcher.utter_message(text=message3, buttons=buttons)

        return []


class ActionUtterWelcomeMessageReturningUser(Action):
    """Welcomes a returning user"""

    def name(self):
        return "action_utter_welcome_message_returning_user"

    def run(self, dispatcher, tracker, domain):
        # Build messages:

        message1 = "Schön, dass Sie wieder da sind!"
        message2 = "Wie kann ich Ihnen helfen?"

        buttons = [
            {
                "title": "Was kannst du nochmal?",
                "payload": "/ask_for_scope_of_application"
            },
            {
                "title": "Tipps zur Bedienung",
                "payload": "Wie kann ich dich bedienen?"  # intent: faq/ask_chatbot_manual
            }
        ]

        # Utter messages:
        dispatcher.utter_message(text=message1, )
        dispatcher.utter_message(text=message2, buttons=buttons)

        return []